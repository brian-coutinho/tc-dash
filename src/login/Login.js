import React from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import CardMedia from "@material-ui/core/CardMedia";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import classNames from 'classnames';

import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import AccountCircle from "@material-ui/icons/AccountCircle";
import Lock from "@material-ui/icons/Lock";

import FormControl from "@material-ui/core/FormControl";

import Typography from "@material-ui/core/Typography";
import "./login.css";
import { Input } from "@material-ui/core";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#E02D2D',
      main: '#E02D2D',
      dark: '#E02D2D',
      contrastText: '#E02D2D',
    },
  }
});

function Login(props) {
  return (
    <div className="card-container">
      <Card className="card">
        <CardMedia
          className="CardMedia"
          image="/src/resources/Red-Background-Color-Collection-40.jpg"  //NOT WORKING 
          title="Contemplative Reptile"
        />


        <MuiThemeProvider theme={theme}>

        <CardContent>

          <Grid container spacing={8} alignItems="flex-end">

            <Grid item>
              <AccountCircle />
            </Grid>
            <Grid item>
              <TextField id="input-with-icon-grid" label="Username" className="UsernameTextField" />
            </Grid>
          </Grid>
       
        </CardContent>
        <CardContent>
          <Grid container spacing={8} alignItems="flex-end">
            <Grid item>
              <Lock />
            </Grid>
            <Grid item>
              <TextField
                id="password-input"
                label="Password"
                className="PasswordTextField"
                type="password"
              />{" "}
            </Grid>
          </Grid>
        </CardContent>
        </MuiThemeProvider>
        <CardContent>
        </CardContent>
        <Button
        variant="contained"
        theme={theme}
        className="ButtonLogin"
      >
        Custom CSS
      </Button>

      </Card>
    </div>
  );
}

export default Login;
